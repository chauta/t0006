

<footer class="l-footer">
	<div class="l-footer__contact">
		<div class="c-footer">
			<div class="l-container">
					<div class="c-footer__image">
						<img class="pc-only" src="assets/image/index/top-44.png" alt="">
						<img class="sp-only" src="assets/image/index/sptop-04.png" alt="">
					</div>
				<div class="c-footer__text">
					<p>
						〒355-0028 埼玉県東松山市箭弓町1-13-16 安福<br class="sp-only">ビル1F　TEL.0493-23-8015<br>
						営業時間：［平日］11:00〜21:00（昼休憩13:00<br class="sp-only">〜14:00）／［土・日・祝］10:00〜19:00　火曜定休<br>
						<span class="pc-only">
							レッスン・フリー利用可能時間：会員・ビジター問わず1時間（準備時間含む）
						</span>
					</p>
				</div>			
			</div>
		</div>
	</div>
	<div class="l-footer__coppy">
		<p>
			Copyright (c) Shin Corporation All Rights Reserved.
		</p>
	</div>
	<div class="l-footer__gototop">
		<div class="l-container l-gototop">
			<a href="">
				<img src="assets/image/index/in-37.png" alt="">
			</a>
		</div>
	</div>
</footer>

<script src="/assets/js/functions.js"></script>
</body>
</html>