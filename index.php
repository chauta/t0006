<?php $id="index";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>


	<section class="p-picmain">
		<div class="c-picmain">
			<div class="l-container">
				<div class="pc-only">
					<div class="c-logobox1">
						<div class="c-logobox1__b1">
							<div class="c-logobox1__img1">
								<img src="assets/image/index/in-02.png" alt="">
							</div>
							<div class="c-logobox1__img1">
								<img src="assets/image/index/in-03.png" alt="">
							</div>
							<div class="c-logobox1__img1">
								<img src="assets/image/index/in-04.png" alt="">
							</div>
						</div>
						<div class="c-logobox1__b2">
							<div class="c-logobox1__img2">
								<img src="assets/image/index/in-07.png" alt="">
							</div>
							<div class="c-logobox1__img3">
								<img src="assets/image/index/in-08.png" alt="">
							</div>
						</div>
					</div>
				</div>
				<div class="sp-only">
					<div class="c-logobox1">
						<div class="c-logobox1__b1">
							<div class="c-logobox1__img1">
								<img src="assets/image/index/sp-02.png" alt="">
							</div>
						</div>
						<div class="c-logobox1__b2">
							<div class="c-logobox1__img2">
								<img src="assets/image/index/in-07.png" alt="">
							</div>
							<div class="c-logobox1__img3">
								<img src="assets/image/index/in-08.png" alt="">
							</div>
						</div>
					</div>
					<div class="c-logobox2">
						<img src="assets/image/index/sp-03.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="l-out">
		<nav class="c-nav1 pc-only">
		    <ul>
		        <li><a href="#">キャンペーン情報</a></li>
			    <li><a href="#">ご利用料金</a></li>
			    <li><a href="#">ご予約</a></li>
			    <li><a href="#">講師紹介</a></li>
			    <li><a href="">店舗のご案内</a></li>
		    </ul>
		</nav>
	</div>
	<section class="p-in2">
		<div class="l-out">
			<div class="c-in2">
				<div class="c-in2__title">
					<div class="c-in2__img1">
						<img class="pc-only" src="assets/image/index/in-11.png" alt="">
						<div class="c-in2__ttl">
							<img src="assets/image/index/in-10.png" alt="">
							<p>
								キャンペーン
							</p>
						</div>
					</div>
				</div>
			<div class="l-container">
				<div class="c-in2__box">
					<div class="c-in2__imgbox">
						<img class="pc-only" src="assets/image/index/top-41.png" alt="">
						<img class="sp-only" src="assets/image/index/sptop-01.png" alt="">
					</div>
					<div class="c-in2__imgbox">
						<img class="pc-only" src="assets/image/index/top-42.png" alt="">
						<img class="sp-only" src="assets/image/index/sptop-02.png" alt="">
					</div>
					<div class="c-in2__imgbox">
						<img class="pc-only" src="assets/image/index/top-43.png" alt="">
						<img class="sp-only" src="assets/image/index/sptop-03.png" alt="">
					</div>
				</div>
			</div>
				<div class="c-in2__btn">
					<div class="c-btn1">
						<a href="">
							<span>見学・体験レッスンのご予約はこちら</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="p-in3">
		<div class="l-container">
			<div class="c-title1">
				<div class="c-title1__img1">
					<img src="assets/image/index/in-21.png" alt="">
				</div>
				<div class="c-title1__circle">
					<p>
						予約制
					</p>
				</div>
				<div class="c-title1__txt">
					<p>
						料金表
					</p>
				</div>
			</div>
			<table class="c-table1">
				<tr>
					<th>事前申し込み＜期間限定＞</th>
				</tr>
				<tr>
					<td>
						<div class="c-table1__left">
							<div class="c-pointer">
								<span>
									体験レッスン
								</span>
							</div>
							<div class="c-table1__price">
								<img src="assets/image/index/in-24.png" alt="">
							</div>
							<div class="c-table1__cost">
								<img src="assets/image/index/in-26.png" alt="">
							</div>
						</div>
						<div class="c-table1__right">
							<div class="c-pointer">
								<span>
									フリー練習
								</span>
							</div>
							<div class="c-table1__price">
								<img src="assets/image/index/in-25.png" alt="">
							</div>
							<div class="c-table1__cost">
								<img src="assets/image/index/in-26.png" alt="">
							</div>
						</div>
					</td>
				</tr>
			</table>
			<table class="c-table1 c-table1--fz2">
				<tr>
					<th>通常料金 ※(税抜)</th>
				</tr>
			</table>
			<table class="c-table2 pc-only">
				<thead>
					<tr>
						<th>&nbsp;</th>
						<th>会員</th>
						<th>ビジター</th>
						<th>フリー練習費</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>⼊会⾦</td>
						<td>￥5,000／1回</td>
						<td><span>−</span></td>
						<td><span>−</span></td>
					</tr>
					<tr>
						<td><p>レギュラー </p><p>全⽇利⽤可</p></td>
						<td>￥10,000（税抜）／⽉額</td>
						<td><span>−</span></td>
						<td><span class="u-red">無料</span></td>
					</tr>
					<tr>
						<td><p>デイタイム</p><p>平⽇昼限定（11:00〜17:00）</p></td>
						<td>￥7,000（税抜）／⽉額</td>
						<td><span>−</span></td>
						<td><span>時間外有料</span></td>
					</tr>
					<tr>
						<td>
							<p>ジュニア </p>
							<p>
									⽊曜⽇限定 17：00〜18：00<br>
									中学3年生まで
							</p>
						</td>
						<td>￥6,000（税抜）／⽉額</td>
						<td><span>−</span></td>
						<td><span>時間外有料</span></td>
					</tr>
					<tr>
						<td>
							<p>ホリデイ</p>
							<p>⼟・⽇・祝のみ終⽇利⽤可</p></td>
						<td>￥8,000（税抜）／⽉額</td>
						<td><span>−</span></td>
						<td><span>時間外有料</span></td>
					</tr>
					<tr>
						<td>
							<p>
								レッスン4
							</p>
							<p>
								レッスン4回<br>
								（スタンプカード制／有効期間：3ヶ月）
							</p>
						</td>
						<td><span>−</span></td>
						<td>￥8,000（税抜）／1セット</td>
						<td><span>有料</span></td>
					</tr>
					<tr>
						<td>
							<p>
								フリー練習
							</p>
							<p>
								空打席があれば利⽤可<br>
								※レッスンは含まず
							</p>
						</td>
						<td>
							<span class="c-break">￥1,000（税抜）／1回</span><br>
							<span class="u-red">プレオープン期間限定 ￥0</span>
						</td>
						<td>
							<span class="c-break">￥1,500（税抜）／1回</span><br>
							<span class="u-red">プレオープン期間限定 ￥0</span>
						</td>
						<td><span>有料</span></td>
					</tr>
					<tr>
						<td>
							<p>
								体験レッスン 
							</p>
							<p>全⽇利⽤可</p>
						</td>
						<td><span>−</span></td>
						<td>￥2,000（税抜）／1回</td>
						<td><span>−</span></td>
					</tr>
				</tbody>
			</table>
			<div class="sp-only c-accord">
				<div>
					<div class="c-accord__title">
						<p>
							入会金
						</p>
					</div>
					<div class="c-accord__panel">
						<table>
							<tbody>
								<tr>
									<th>⼊会⾦</th>
									<td>￥5,000（税抜）／1回</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div>
					<div class="c-accord__title">
						<p>
							レギュラー
						</p>
						<span>
							全⽇利⽤可
						</span>
					</div>
					<div class="c-accord__panel">
						<table>
							<tbody>
								<tr>
									<th>会員</th>
									<td>￥10,000（税抜）／⽉額</td>
								</tr>
								<tr>
									<th>ビジター</th>
									<td>-</td>
								</tr>
								<tr>
									<th>フリー練習費</th>
									<td>無料</td>
								</tr>
								<tr>
									<th>打席料・ボール代</th>
									<td>無料</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div>
					<div class="c-accord__title">
						<p>
							デイタイム
						</p>
						<span>
							平⽇昼限定（11:00〜17:00）
						</span>
					</div>
				</div>
				<div>
					<div class="c-accord__title">
						<p>
							ジュニア
						</p>
						<span>
							⽊曜⽇限定 17：00〜18：00<br>
							中学3年生まで
						</span>
					</div>
				</div>
				<div>
					<div class="c-accord__title">
						<p>
							ジュニア
						</p>
						<span>
							⽊曜⽇限定 17：00〜18：00<br>
							中学3年生まで
						</span>
					</div>
				</div>
				<div>
					<div class="c-accord__title">
						<p>
							ホリデイ
						</p>
						<span>
							⼟・⽇・祝のみ終⽇利⽤可
						</span>
					</div>
				</div>
				<div>
					<div class="c-accord__title">
						<p>
							レッスン4
						</p>
						<span>
							レッスン4回（スタンプカード制<br>
							／有効期間：3ヶ月）
						</span>
					</div>
				</div>
				<div>
					<div class="c-accord__title">
						<p>
							フリー練習
						</p>
						<span>
							空打席があれば利⽤可<br>
							※レッスンは含まず
						</span>
					</div>
				</div>
				<div>
					<div class="c-accord__title">
						<p>
							体験レッスン
						</p>
						<span>
							全⽇利⽤可
						</span>
					</div>
				</div>
			</div>
			<div class="c-in3">
				<div class="c-in3__con">
					<div class="c-lab1">
							<span>レッスン・フリー利用可能時間</span>
					</div>
					<div class="c-in3__txt">
						1時間（準備時間含む、会員・ビジター問わず）
					</div>
				</div>
				<div class="c-in3__con">
					<div class="c-lab1 c-lab1--cl2">
						<span>学生割引</span>
					</div>
					<div class="c-in3__txt">
						上記料金より50％OFF（会員カテゴリのみ適用※フリー練習含まず）
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="p-in4">
		<div class="c-in4">
			<div class="l-container">
				<div class="c-in4__txt1">
					見学・体験のご予約もこちらから!
				</div>
				<div class="c-in4__txt2">
					<img src="assets/image/index/in-39.png" alt="">
					<span>
						レッスンWeb予約
					</span>
				</div>
				<div class="c-in4__txt3">
					<p>
						当スクールは予約制となっております。<br>
						お電話、または予約システムよりご予約をお願い致します。
					</p>
				</div>
				<div class="c-in4__btn">
					<div class="c-in4__contact">
						<div class="c-btn2">
							<span class="c-book">ご予約はこちら</span>
						</div>
					</div>
					<div class="c-in4__contact">
						<div class="c-btn2">
							<p>
								お電話でのご予約<br>
							</p>
							<img src="assets/image/index/in-40.png" alt="">
						</div>
						<div class="c-in4__sub">
							<p>
								平日11:00～21:00（昼休憩13:00～14:00）／<br>
								土・日・祝10:00～19:00<br>
								定休日 火曜日<br>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="p-in5">
		<div class="l-container">
			<div class="c-in5">
				<div class="p-in5intro">
					<div class="c-title1">
						<div class="c-title1__img1">
							<img src="assets/image/index/in-22.png" alt="">
						</div>
						<div class="c-title1__circle">
							<img src="assets/image/index/in-29.png" alt="">
						</div>
						<div class="c-title1__txt">
							<p>
								講師紹介
							</p>
						</div>
					</div>
					<div class="c-in5__profile">
						<div class="c-pro">
							<div class="c-pro__avatar">
								<img src="assets/image/index/in-06.png" alt="">
							</div>
							<div class="c-pro__info">
								<div class="c-pro__txt1">
									<h6 class="pc-only">
										矢島　嘉彦
									</h6>
									<h6 class="sp-only">
										講師名が入ります
									</h6>
									<p>
										1,000人以上のレッスン経験があり、初心者から上級者までそれぞれの悩みや疑問を解決し指
										導します。
									</p>
								</div>
								<div class="c-pro__txt2">
									<ul>
										<li>
											公益社団法人日本プロゴルフ協会
										</li>
										<li>
											高校生からゴルフを始め、2006年にティーチングプロとして社団法人日本プロゴル
											フ協会の会員となる
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="p-in5guild">
					<div class="c-title1">
						<div class="c-title1__img1">
							<img src="assets/image/index/in-23.png" alt="">
						</div>
						<div class="c-title1__circle">
							<img src="assets/image/index/in-29.png" alt="">
						</div>
						<div class="c-title1__txt">
							<p>
								店舗のご案内
							</p>
						</div>
					</div>
					<div class="c-in5__dumimg">
						<div class="c-imgdummy">
							<img src="assets/image/index/in-30.png" alt="">
							<p>
								ダミー画像
							</p>
						</div>
						<div class="c-imgdummy">
							<img src="assets/image/index/in-31.png" alt="">
							<p>
								ダミー画像
							</p>
						</div>
						<div class="c-imgdummy">
							<img src="assets/image/index/in-32.png" alt="">
							<p>
								ダミー画像
							</p>
						</div>
						<div class="c-imgdummy">
							<img src="assets/image/index/in-33.png" alt="">
							<p>
								ダミー画像
							</p>
						</div>
						<div class="c-imgdummy">
							<img src="assets/image/index/in-34.png" alt="">
							<p>
								ダミー画像
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="p-in6">
		<div class="l-container">
			<div class="c-in6">
				<div class="c-in6__left">
					<div class="c-title1">
						<div class="c-title1__txt">
							<p>
								アクセスマップ
							</p>
						</div>
					</div>
					<div class="c-in6__map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d806.5933742402104!2d139.40230065831912!3d36.03558957096929!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018d5210bdb3f87%3A0xe843e88a64e460b5!2z44Kk44Oz44OJ44Ki44K044Or44OV44K544Kv44O844OrQVJST1dT5p2x5p2-5bGx5bqX!5e0!3m2!1sen!2s!4v1529303658892" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="c-in6__right">
					<div class="c-in6__mapinfo">
						<table class="c-table3">
								<tr>
									<th>
										<img src="assets/image/index/icon1.png" alt="">
										営業時間

									</th>
									<td>
										平日11:00〜21:00(昼休憩13:00〜14:00)<br class="sp-only">
										土・日・祝10:00〜19:00
									</td>
								</tr>
								<tr>
									<th>
										<img src="assets/image/index/icon2.png" alt="">
										定休日
									</th>
									<td>火曜日</td>
								</tr>
								<tr>
									<th>
											<img src="assets/image/index/icon3.png" alt="">
											レッスン・フリー<br class="pc-only">
											利用可能時間
									</th>
									<td>
										会員・ビジター問わず1時間(準備時間含む)
									</td>
								</tr>
								<tr>
									<th>
											<img src="assets/image/index/icon4.png" alt="">
											TEL
									</th>
									<td>0493-23-8015</td>
								</tr>
								<tr>
									<th>
										<img src="assets/image/index/icon5.png" alt="">
										住所
									</th>
									<td>
										〒355-0028<br>
										埼玉県東松山市箭弓町1-13-16 安福ビル1F
									</td>
								</tr>
						</table>
					</div>
				</div>
			</div>
	</div>
	</section>
	<section class="p-in7">
		<div class="l-container">
			<div class="c-title2">
				<div class="c-title2__img">
					<img src="assets/image/index/in-35.png" alt="">
				</div>
				<p>
					お知らせ
				</p>
			</div>
			<div class="c-in7">
				<p>
					 2018年0月0日　<br class="sp-only">インドアゴルフスクール ARROWS 東松山店 サイト<br class="sp-only">公開しました。
				</p>
			</div>
		</div>
	</section>
	
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>